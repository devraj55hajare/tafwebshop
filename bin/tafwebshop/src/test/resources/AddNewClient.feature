Feature: Verify Add new Client functionality 
@AddNewClient 
Scenario: Verify user is able to add new client 
	Given a member login to the XPlan application 
	When a member click on add button and select "Add a New Client" option 
	Then verify member navigated to select client type page 
	And a member navigate to "Basic information" page 
	And a member fills required basic information 
	And a member navigate to "Contact Details" page 
	And verify new client is added successfully 
 