package com.test.base;

import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class RunDemo extends PageBase {
	By linkSkipAd = By.xpath("//img[@alt='Skip Ad']");
	By buttonMenu = By.xpath("//a[@class='dt-mobile-menu-icon']");

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver",
				"E:\\DevrajWorkspace\\AutomationFramework\\drivers\\chromedriver.exe");
		
		for (int i = 0; i < 100; i++) {
			WebDriver driver = new ChromeDriver();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			new RunDemo().runTest(driver);
			System.out.println("run completed : "+ (i+1));
		}

	}

	public void runTest(WebDriver driver) {

		
		try {
			driver.get("http://sprysphere.com/HfBN");
			waitForElementWithDriver(driver,linkSkipAd).click();
			Thread.sleep(2000);
			Set<String> wh = driver.getWindowHandles();
			Iterator<String> itr = wh.iterator();
			String parent = itr.next();
			String child = itr.next();
			Thread.sleep(5000);
			driver.switchTo().window(child);
			driver.close();
			driver.switchTo().window(parent);
			driver.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} /*finally{
			
			if(driver.getWindowHandles().size()==1) {
				driver.close();
			}
			if(driver.getWindowHandles().size()==2) {
				Set<String> wh = driver.getWindowHandles();
				Iterator<String> itr = wh.iterator();
				String parent = itr.next();
				String child = itr.next();
				driver.switchTo().window(child);
				driver.close();
				driver.switchTo().window(parent);
				driver.close();
			}*/
		

	}

}
