package com.test.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.test.base.PageBase;

public class HomePage extends PageBase {

	By buttonAdd = By.xpath("a[href='#']");
	By listMenuOptions = By.xpath("//div[@class='header-links']/ul/li");
	
	public HomePage selectAdd() {
		waitForElement(buttonAdd).click();
		return new HomePage();
	}

	public void selectMenuOnHomePage(String option) {
		for (WebElement element : waitForElements(listMenuOptions)) {
			if (element.getText().equalsIgnoreCase(option)) {
				element.click();
				break;
			}
		}
		
	}

}
