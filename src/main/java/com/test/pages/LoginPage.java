package com.test.pages;

import org.openqa.selenium.By;
import com.test.base.PageBase;

public class LoginPage extends PageBase {

	By inputUserName = By.id("Email");
	By inputPassword = By.id("Password");
	By buttonLogIn = By.xpath("//input[@class='button-1 login-button']");
	By logoutlink=By.xpath("//a[@class='ico-logout']");
    By emailLink=By.xpath("//div[@class='header-links']//a[@href='/customer/info']");
	
    public void loginToApplication(String username, String password) {
		waitForElement(inputUserName).sendKeys(username);
		waitForElement(inputPassword).sendKeys(password);
		waitForElement(buttonLogIn).click();

	}

	public boolean verifyNavigateToLogin() {
		try {
			return waitForElement(buttonLogIn).isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}
	
	public boolean verifylogOut() {
		try {
			return waitForElement(logoutlink).isDisplayed();
			}catch(Exception e) {
				return false;
			}
	}
	
	public boolean verifyemaillink() {
		try {
			return waitForElement(emailLink).isDisplayed();
			}catch(Exception e) {
				return false;
			}
	}

}