package com.test.pages;

import org.openqa.selenium.By;

import com.test.base.PageBase;

public class RegisterPage extends PageBase {
	By refirstName = By.xpath("//input[@id='FirstName']");
	By relastName = By.xpath("//input[@id='LastName']");
	By remail = By.xpath("//input[@id='Email']");
	By passwordse = By.xpath("//input[@id='Password']");
	By confirmpass = By.xpath("//input[@id='ConfirmPassword']");
	By registerButton = By.id("register-button");
	By continueButton = By.xpath("//input[@class='button-1 register-continue-button']");
	By LogOutLink=By.xpath("//a[@class='ico-logout']");
	By emailNameLink=By.xpath("//div[@class='header-links']//a[@class='account']");

	public void registerSection(String firstname, String lastname, String email) {
		waitForElement(refirstName).sendKeys(firstname);
		waitForElement(relastName).sendKeys(lastname);
		waitForElement(remail).sendKeys(email);
	}

	public void passwordSection(String password, String confirmPassword) {
		waitForElement(passwordse).sendKeys(password);
		waitForElement(confirmpass).sendKeys(confirmPassword);
		waitForElement(registerButton).click();
		waitForElement(continueButton).click();

	}

	public boolean verifyNavigateToregisterPage() {
		try {
			return waitForElement(registerButton).isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	public boolean verifyNavigateSuccefullPage() {
		String expRes = waitForElement(By.xpath("//div[@class='result']")).getText();
		String actRes = "Your registration completed";
		return expRes.equals(actRes);

	}
	
	public boolean verifyLogOut() {
		try {
			return waitForElement(LogOutLink).isDisplayed();
			}catch(Exception e) {
				return false;
			}
	}
	
	public boolean verifyEmailLink() {
		try {
			return waitForElement(emailNameLink).isDisplayed();
			}catch(Exception e) {
				return false;
			}
	}


}
