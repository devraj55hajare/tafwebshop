package com.test.map;

import java.util.HashMap;
import java.util.Map;

public class DataMapRepo {

	private static Map<Long, DataMaps> map = new HashMap();

	public static DataMaps getMap() {

		DataMaps returnMap = map.get(Thread.currentThread().getId());

		if (returnMap == null) {
			loadDataMap();
		}

		return map.get(Thread.currentThread().getId());
	}

	public static void loadDataMap() {
		DataMaps dataMaps = new DataMaps();
		map.put(Thread.currentThread().getId(), dataMaps);
	}

}
