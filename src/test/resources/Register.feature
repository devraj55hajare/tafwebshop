Feature: This feature to test Register functionality

@LoginTest_02
  Scenario: Verify user is able to login successful
    Given user launch demo web shop application
    When user select "Register" option from menu
    Then verify user is navigated to register page
    Then user register with "Male" or "Female" gender 
    Then user register with "john" firstname,"john1" Lastname and "john.support34@gmail.com" email
    Then user register password section with"support1" Password and "support1" Confirm password
    Then user select "Continue" button
    Then verify "Log out" is present in menu
    Then verify "john.support34@gmail.com" emailname is present in menu