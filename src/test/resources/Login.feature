Feature: This feature to test login functionality

  @LoginTest_011
  Scenario: Verify user is able to login successful
    Given user launch demo web shop application
    When user select "Log in" option from menu
    Then verify user is navigated to login page
    Then user enter "email" in email text box
    Then user enter "Password" in text box
    Then user click on "Log in" button
    Then verify "Log out" is present in menu
    Then verify "email name" link is present in menu
    
    
    @LoginTest_01
  Scenario: Verify user is able to login successful
    Given user launch demo web shop application
    When user select "Log in" option from menu
    Then verify user is navigated to login page
    Then user login with "john.support1@gmail.com" username and "support1" passowrd
    Then verify "Log out" is present in menu
    Then verify "email name" link is present in menu
