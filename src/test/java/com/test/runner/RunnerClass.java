package com.test.runner;

import org.junit.runner.RunWith;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
		plugin = {"pretty","html:target/cucumber"},
		features = "src/test/resources", 
		glue = "com.test.stepdef", 
		tags = { "@LoginTest_02" },
		strict = true)
public class RunnerClass {

}
