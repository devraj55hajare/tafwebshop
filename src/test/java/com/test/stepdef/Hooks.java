package com.test.stepdef;

import com.test.base.WebDriverBase;
import com.test.map.DataMapRepo;
import com.test.map.DataMaps;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;

public class Hooks {

	@Before
	public void onStart(Scenario scenario) {
		DataMaps.map.put("currentScenario", scenario);
		
	}

	@After
	public void onTearDown() {
		WebDriverBase.getDriverInstance().quit();
	}

}
