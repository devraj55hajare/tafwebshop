package com.test.stepdef;

import org.junit.Assert;

import com.test.map.DataMapRepo;
import com.test.map.DataMaps;
import com.test.pages.HomePage;
import com.test.pages.LoginPage;
import com.test.pages.RegisterPage;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class WebStepDef {

	@Given("^user launch demo web shop application$")
	public void loginToApplication() {
		HomePage homePage = new HomePage();
		homePage.launchApplication();
	}

	@When("user select {string} option from menu")
	public void selectMenuOnHomePage(String option) {
		HomePage homePage = new HomePage();
		homePage.selectMenuOnHomePage(option);
	}

	@Then("verify user is navigated to login page")
	public void verifyNavigateToLogin() {
		LoginPage loginPage = new LoginPage();
		Assert.assertTrue("User did not navigated to login page", loginPage.verifyNavigateToLogin());
	}

	@Then("user login with {string} username and {string} passowrd")
	public void loginToApp(String username, String password) {
		LoginPage loginPage = new LoginPage();
		loginPage.loginToApplication(username, password);
	}

	@Then("verify {string} link is present in menu")
	public void emailPresent(String email) {
		LoginPage loginPage = new LoginPage();
		Assert.assertTrue("email is not present in top", loginPage.verifyemaillink());
		DataMaps.map.get("currentScenario").log("emailname link is present after successful login");
	}

	@Then("verify user is navigated to register page")
	public void navigateRagisterPage() {
		RegisterPage registerpage = new RegisterPage();
		Assert.assertTrue("User did not navigated to register page", registerpage.verifyNavigateToregisterPage());
	}

	@Then("user register with {string} or {string} gender")
	public void genderradio(String male, String female) {

	}

	@Then("user register with {string} firstname,{string} Lastname and {string} email")
	public void registerSection(String firstname, String lastName, String email) {
		RegisterPage registerpage = new RegisterPage();
		registerpage.registerSection(firstname, lastName, email);

	}

	@Then("user register password section with{string} Password and {string} Confirm password")
	public void passwordsection(String password, String confirmpassword) {
		RegisterPage registerpage = new RegisterPage();
		registerpage.passwordSection(password, confirmpassword);
	}

	

	@Then("user select {string} button")
	public void selectContinuebutton(String contiue) {

	}

	@Then("verify {string} is present in menu")
	public void verifyLogOutLink(String logout) {
		RegisterPage registerpage = new RegisterPage();
		Assert.assertTrue("logout is not present in top", registerpage.verifyLogOut());
		DataMaps.map.get("currentScenario").log("Logout link is present after successful login");
	}

	@Then("verify {string} emailname is present in menu")
	public void verifyEmail(String emaillink) {
		RegisterPage registerpage = new RegisterPage();
		Assert.assertTrue("email is not present in top", registerpage.verifyEmailLink());
		DataMaps.map.get("currentScenario").log("email link is present after successful register");
	}

}
